#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define N 100
int main()
{
	srand(time(0));
	int arr[N];
	int i, len=1;
	int elem,max=1;
	for (i = 0;i < N;i++) 
		arr[i] = (rand() % 201) - 100;
	for (i = 1;i <= N; i++)
	{
		if (arr[i] == arr[i - 1])
			len++;
		else
		{
			if (len > max)
			{
				max = len;
				elem = arr[i];
			}
			len= 1;
		}	
	}
	printf("%d\n", max);
	for (i=0;i<len;i++)
	{	
		printf("%d", elem);
		if (i < len - 1)
			printf(", ");
	}
	return 0;
}